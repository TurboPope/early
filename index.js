var restify = require('restify');
var logger = require('morgan');
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var fs = require('fs');

// Self-Signed Certificate created for localhost with: http://www.selfsignedcertificate.com/
var options = {
	key: fs.readFileSync('cert/self-signed/localhost.key'),
	certificate: fs.readFileSync('cert/self-signed/localhost.cert'),
	name: "early"
};

var app = restify.createServer(options);
passport.use(new BasicStrategy({},
	function(username, password, callback) {
		return callback(null, {username: "testuser", password: "testpassword"});
	}
));

app.use(logger('dev'));
app.use(passport.initialize());

app.get('/res', passport.authenticate('basic', {session: false}),
	function(request, response) {
		response.json("Hello client!");
	}
);

app.listen(8080, function() {
	console.log('%s listening at %s', app.name, app.url);
});